package main.rootpanels.windows;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import main.dao.AccountsDAO;
import main.rootpanels.LeftVBox;
import main.rootpanels.TopHBox;
import main.variables.Variables;

/**
 * Page de connexion
 */
public class Login {
    static Label errorMessage;

    /**
     * Création de la page
     * @param borderPane Panneau principal
     */
    public void createLoginHBox(BorderPane borderPane){
        HBox center = new HBox();
        BorderPane.setAlignment(center, Pos.TOP_CENTER);

        VBox formulaireConnexion = createFormulaireConnexion(borderPane);
        center.getChildren().add(formulaireConnexion);
        center.setAlignment(Pos.BASELINE_CENTER);
        borderPane.setCenter(center);
    }

    /**
     * Création du formulaire de connexion
     * @param borderPane Panneau principal
     * @return Retourne le formulaire
     */
    private VBox createFormulaireConnexion(BorderPane borderPane){
        VBox vbox = new VBox();
        vbox.setAlignment(Pos.CENTER);
        HBox.setMargin(vbox, new Insets(90, 0, 90, 0));
        vbox.setPadding(new Insets(10, 50, 30, 50));
        vbox.getStyleClass().add("login-container");

        errorMessage = new Label();

        Text title = new Text("Connexion");
        title.setFont(Font.font(Variables.policeTitre, FontWeight.NORMAL, Variables.taillePoliceTitre));

        HBox loginBox = new HBox();
        VBox.setMargin(loginBox, new Insets(20, 0, 0, 0));
        loginBox.setAlignment(Pos.BASELINE_RIGHT);
        Label userName = new Label("Nom d'utilisateur :");
        userName.setFont(Font.font(Variables.policeIntermediaire, FontWeight.NORMAL, Variables.taillePoliceIntermediaire));
        HBox.setMargin(loginBox, new Insets(0, 10, 0, 0));
        TextField userTextField = new TextField();
        userTextField.setPrefWidth(220);
        loginBox.getChildren().addAll(userName, userTextField);

        HBox pwBox = new HBox();
        VBox.setMargin(pwBox, new Insets(15, 0, 0, 0));
        pwBox.setAlignment(Pos.BASELINE_RIGHT);
        Label pw = new Label("Mot de passe :");
        pw.setFont(Font.font(Variables.policeIntermediaire, FontWeight.NORMAL, Variables.taillePoliceIntermediaire));
        HBox.setMargin(pw, new Insets(0, 10, 0, 0));
        PasswordField pwField = new PasswordField();
        pwField.setPrefWidth(220);
        pwBox.getChildren().addAll(pw, pwField);

        Button validateButton = new Button("Valider");
        validateButton.getStyleClass().add("btn-primary");
        VBox.setMargin(validateButton, new Insets(20, 0, 0, 0));
        EventHandler<ActionEvent> actionEventEventHandler = event -> {
            AccountsDAO accountsDAO = new AccountsDAO();
            String resultEssai = accountsDAO.essaiLogin(userTextField, pwField);
            if(resultEssai.equals("Success")){
                //Affiche le nom dans la barre top
                TopHBox.nom.setText(Variables.userName);
                TopHBox.deco.setVisible(true);
                LeftVBox.connected(borderPane);
            }else{
                errorMessage.setText(resultEssai);
                VBox.setMargin(errorMessage, new Insets(0, 0, 15, 0));
                errorMessage.setFont(Font.font(Variables.policeIntermediaire, FontWeight.NORMAL, Variables.taillePoliceIntermediaire));
                errorMessage.setTextFill(Color.web("#dc3545"));
                errorMessage.setWrapText(true);
                errorMessage.getStyleClass().add("labelspacing");
                errorMessage.setTextAlignment(TextAlignment.CENTER);
                errorMessage.setMaxWidth(loginBox.getWidth());
            }
        };
        validateButton.setOnAction(actionEventEventHandler);
        pwField.addEventHandler(KeyEvent.KEY_PRESSED, ev -> {
            if (ev.getCode() == KeyCode.ENTER) {
                validateButton.fire();
                ev.consume();
            }
        });
        vbox.getChildren().addAll(errorMessage, title, loginBox, pwBox, validateButton);

        return vbox;
    }
}
